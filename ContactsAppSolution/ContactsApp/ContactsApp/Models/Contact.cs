﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsApp.Models
{
    public class Contact 
    {
        public string Name { get; set; }
 
        public string Website { get; set; }

        public bool BestFriend { get; set; }
    }
}
