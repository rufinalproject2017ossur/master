﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ContactsApp.ViewModels
{
    public class AddContactViewModel : INotifyPropertyChanged
    {
        private string _name = "Derp Derpson";
        private string _website = "http://www.mbl.is";
        private bool _isBusy;
        private bool _bestFriend;
        public Command LunchWebsiteCommand { get; }
        public Command SaveContactCommand { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public AddContactViewModel()
        {
            _isBusy = false;
            LunchWebsiteCommand = new Command(LunchWebsite, () => !IsBusy);
            SaveContactCommand = new Command(async ()=> await SaveContact(), () => !IsBusy);
        }

        public bool BestFriend
        {
            get
            {
                return _bestFriend;
            }
            set
            {
                _bestFriend = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayMessage));
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayMessage));
            }
        }

        public string Website
        {
            get { return _website; }
            set
            {
                _website = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayMessage));
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
                LunchWebsiteCommand.ChangeCanExecute();
                SaveContactCommand.ChangeCanExecute();
            }
        }

        public string DisplayMessage => $"Your new friend is named {Name} and {(BestFriend ? "is" : "is not")} your best friend.";


        private void LunchWebsite()
        {
            try
            {
                Device.OpenUri(new Uri(_website));
            }
            catch
            {
                
            }
        }

        async Task SaveContact()
        {
            IsBusy = true;
            await Task.Delay(4000);
            IsBusy = false;
            await Application.Current.MainPage.DisplayAlert("Save", "Contact has been saved", "OK");
        }

    }
}
